import Vue from 'vue'
import Router from 'vue-router'
import Fill from '../views/Fill'
import Settings from '../views/Settings'
import Journey from '../views/Journey'
import Experimental from '../views/Experimental'
import Practice from '../views/Practice'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Fill
    },
    {
      path: '/settings',
      component: Settings,
      name: 'settings'
    },
    {
      path: '/journey',
      redirect: '/journey/arpeggiator/setup'
    },
    {
      name: 'journey',
      path: '/journey/:game?/:gameOption?/:gameOption2?',
      component: Journey
    },
    {
      path: '/practice',
      redirect: '/practice/arpeggiator/setup'
    },
    {
      name: 'practice',
      path: '/practice/:game?/:gameOption?/:gameOption2?',
      component: Practice
    },
    {
      path: '/experimental',
      component: Experimental
    }
  ]
})
