import { octave } from './_tones'
import { chords } from './_chords'


const doesChordBelong = (chordNotes, scaleNotes, chordRoot) => {
  // return true or false based all notes of chordNotes ( transposed by chordRoot ) belonging to the scale
  // chordNotes = array []
  // scaleNotes = array []
  // chordRoot = integer, index of note in scaleNotes that will be considered a chordRoot
  // --> chordNotes[0] + scaleNotes[chordRoot] is the chord root (chordNotes[0] should be always 0)
  const selectedRootNote = chordNotes[0] + chordRoot
  let noteBelongs = 1
  for (let i = chordNotes.length - 1; i > 0; i--) {
    // console.log(`i is equal ${i} - ${selectedRootNote} scaleNotes[chordRoot]  ${scaleNotes[chordRoot]} -- scaleNotes being ${scaleNotes} and chordRoot being ${chordRoot} and ${chordNotes[i] + selectedRootNote} which turns to ${ (chordNotes[i] + selectedRootNote) % 12}`)
    noteBelongs += typeof scaleNotes.find(el => el === (chordNotes[i] + selectedRootNote) % 12) === 'number'
  }
  return noteBelongs === chordNotes.length
}

const makeScaleWithChords = (scaleType, chordsArray) => {
  // for each scale
  const chordArray = chordsArray.map((chordType) => {
    const scaleRoots = scaleType.notes.filter((rootNote) => doesChordBelong(chordType.notes, scaleType.notes, rootNote))
    return Object.assign({}, chordType, { scaleRoots })
  })
    // and for each chord type
    // we go through scale notes as different roots

  const scaleWithChords = Object.assign({}, scaleType, { chordArray })
  return scaleWithChords
}

const makeScales = () => {
  const scaleSource = [
    /*
    MAJOR
    G,A,B,C,D,E,F#
    3,5,7,8,10,12,14
    0,2,4,5,7,9,11

    G,G#,A,A#,B,C,C#,D,D#,E,F,F#
    0,  ,2,  ,4,5,  ,7,  ,9, ,11

    G    , A    , B     , C    , D    , E     
    0 + 7, 2 + 9, 4 + 11, 5 + 0, 7 + 2, 9 + 4
    */
    {
      name: 'major',
      notes: [0, 2, 4, 5, 7, 9, 11]
    },
    /*
    PENTATONIC MINOR
    A,C,D,E,G
    5,8,10,12,15
    0,3,5,8,10
     */
    {
      name: 'pentatonic minor',
      notes: [0, 3, 5, 7, 10]
    },

    /*
    PENTATONIC MAJOR
    */
    {
      name: 'pentatonic major',
      notes: [0, 2, 4, 7, 9]
    },

    /*
    PENTATONIC BLUES
    */
    {
      name: 'pentatonic blues',
      notes: [0, 3, 5, 6, 7, 10]
    },

    /*
    HARMONIC MINOR
    */
    {
      name: 'harmonic minor',
      notes: [0, 2, 3, 5, 7, 8, 11]
    },

    /*
    MELODIC MINOR
    */
    {
      name: 'melodic minor',
      notes: [0, 2, 3, 5, 7, 9, 11]
    },

    /*
    WHOLE TONE
    */
    {
      name: 'whole tone',
      notes: [0, 2, 4, 6, 8, 10]
    },

    /*
    PENTATONIC NEUTRAL
    */
    {
      name: 'pentatonic neutral',
      notes: [0, 2, 5, 7, 10]
    },

    /*
    OCTATONIC (H-W)
    */
    {
      name: 'octatonic (H-W)',
      notes: [0, 1, 3, 4, 6, 7, 9, 10]
    },

    /*
    OCTATONIC (W-H)
    */
    {
      name: 'octatonic (W-H)',
      notes: [0, 2, 3, 5, 6, 8, 9, 11]
    },

    /*
    IORIAN
    */
    {
      name: 'ionian',
      notes: [0, 2, 4, 5, 7, 9, 11]
    },

    /*
    DORIAN
    */
    {
      name: 'dorian',
      notes: [0, 2, 3, 5, 7, 9, 10]
    },

    /*
    PHYGRIAN
    */
    {
      name: 'phrygian',
      notes: [0, 1, 3, 5, 7, 8, 10]
    },

    /*
    LYDIAN
    */
    {
      name: 'lydian',
      notes: [0, 2, 4, 6, 7, 9, 11]
    },

    /*
    MIXOLYDIAN
    */
    {
      name: 'mixolydian',
      notes: [0, 2, 4, 5, 7, 9, 10]
    },

    /*
    AEOLIAN
    */
    {
      name: 'aeolian',
      notes: [0, 2, 3, 5, 7, 8, 10]
    },

    /*
    LOCRIAN
    */
    {
      name: 'locrian',
      notes: [0, 1, 3, 4, 5, 6, 8, 10]
    },

    /*
    BEBOP MAJOR
    */
    {
      name: 'bebop major',
      notes: [0, 2, 4, 5, 7, 8, 9, 11]
    },

    /*
    BEBOP MINOR
    */
    {
      name: 'bebop minor',
      notes: [0, 2, 3, 4, 5, 7, 9, 10]
    }
  ]

  const scalesWithChords = scaleSource.map(scaleType => makeScaleWithChords(scaleType, chords))
  const rootSpecific = () => octave.reduce((arr, val, i) => {
    arr[val] = scalesWithChords.map(scale => {
      return {
        root: val,
        rootIndex: i,
        name: scale.name,
        notes: scale.notes,
        chordArray: scale.chordArray
      }
    })
    return arr
  }, {})
  return {
    /*
    CHROMATIC
    */
    all: makeScaleWithChords({
      name: 'chromatic / all',
      notes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    }, chords),
    rootSpecific: rootSpecific()
  }
}

export const scales = makeScales()
