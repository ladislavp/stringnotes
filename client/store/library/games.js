import { library } from './_library'
import { getChordObject } from '../helpers/_chordHelpers'
import { arpeggiatorSettings } from './_gameSettings'
const fretboardSettings = {
  levels: [
    {
      gameTime: 60,
      gameMistakes: -2,
      noteTime: 30,
      noteMistakes: 3,
      goal: 2
    },
    {
      gameTime: 60,
      gameMistakes: -2,
      noteTime: 20,
      noteMistakes: 3,
      goal: 3
    },
    {
      gameTime: 60,
      gameMistakes: -2,
      noteTime: 4,
      noteMistakes: 3,
      goal: 8
    }
  ]
}
const getRandomInt = (rangeFromZero, lastInt = -1) => {
  let newInt = lastInt
  while (newInt === lastInt) {
    newInt = Math.floor(Math.random() * (rangeFromZero))
  }
  return newInt
}

const getNoteName = (index) => library.octave[index % 12]

const getStringName = (stringIndex) => {
  return library.tones[stringIndex].note
}

export const getScaleNotes = (scale) => {
  const rootIndex = scale.rootIndex || 0
  return scale.notes.map(el => getStringName(rootIndex + el))
}

const makeNoteArray = (chordObject) => {
  const rootIndex = library.octave.findIndex(el => el === chordObject.root)
  return chordObject.notes.map(el => {
    return {
      name: getNoteName(rootIndex + el),
      time: -1,
      index: -1
    }
  })
}

const makeNotePool = (tuning) => {
  const strings = tuning.strings
  const stringArray = strings.map(el => el % 12)
  return [...new Set(stringArray)]
}


const fretboardTurn = (
  currentSetup,
  gameSettings,
  oldTurn,
  userInput = 'undefined',
  gameLevel = 0
  ) => {
  const notePool = makeNotePool(currentSetup.tuning)
  const getLevel = (level) => {
    const gameLevelNew = Math.min(level, fretboardSettings.levels.length - 1)
    return fretboardSettings.levels[gameLevelNew]
  }

  const levelUp = (turnObj) => {
    const gameLevelNew = turnObj.gameLevel + 1
    return Object.assign({}, turnObj, {
      time: {
        allowed: getLevel(gameLevelNew).gameTime,
        current: getLevel(gameLevelNew).gameTime,
        ticked: turnObj.time.ticked
      },
      allTurns: turnObj.allTurns.concat(turnObj.turns),
      turns: [],
      gameLevel: gameLevelNew,
      gameLevelObj: getLevel(gameLevelNew)
    })
  }

  const makeNewNoteObj = (gameLevelObj, oldNoteObj = 'undefined') => {
    if (oldNoteObj !== 'undefined') {
      let newNote = gameSettings.selected[getRandomInt(gameSettings.selected.length)]
      while (gameSettings.selected.length > 1 && newNote === oldNoteObj.note) {
        newNote = gameSettings.selected[getRandomInt(gameSettings.selected.length)]
      }
      return {
        note: newNote,
        string: getRandomInt(notePool.length, oldNoteObj.string),
        time: {
          allowed: gameLevelObj.noteTime,
          current: gameLevelObj.noteTime
        },
        tries: [],
        status: 'inProgress' // correct, incorrect, inProgress
      }
    }
    return {
      note: gameSettings.selected[getRandomInt(gameSettings.selected.length)],
      string: getRandomInt(notePool.length),
      time: {
        allowed: gameLevelObj.noteTime,
        current: gameLevelObj.noteTime
      },
      tries: [],
      status: 'inProgress' // correct, incorrect, inProgress
    }
  }

  const makeNewTurn = () => {
    const gameLevelObj = getLevel(gameLevel)
    return {
      time: {
        allowed: gameLevelObj.gameTime,
        current: gameLevelObj.gameTime,
        ticked: 0
      },
      notePool,
      allTurns: [],
      turns: [],
      currentNoteObj: makeNewNoteObj(gameLevelObj),
      gameLevel,
      selected: [...gameSettings.selected],
      gameLevelObj,
      score: 0,
      scoreAdded: 0,
      status: 'start'
    }
  }
  if ((oldTurn.status === 'start' || oldTurn.status === 'win') && userInput === 'undefined') return oldTurn
  let newTurn
  if (oldTurn === 'undefined' || userInput === 'reset') {
    newTurn = makeNewTurn()
    return newTurn
  }

  newTurn = Object.assign({}, oldTurn)
  newTurn.scoreAdded = 0
  // console.log(newTurn)
  if (userInput === 'undefined') {
    newTurn.time.current--
    newTurn.time.ticked++
    if (newTurn.time.current < 0) {
      newTurn.time.current = 0
      newTurn.status = 'loss'
      return newTurn
    }
    newTurn.currentNoteObj.time.current--
    if (newTurn.currentNoteObj.time.current === -1) {
      newTurn.currentNoteObj.status = 'loss'
      newTurn.turns.push(newTurn.currentNoteObj)
      newTurn.currentNoteObj = makeNewNoteObj(newTurn.gameLevelObj, newTurn.currentNoteObj)
    }
    return newTurn
  }
  // handle user input below
  
  const inputNote = getNoteName(currentSetup.tuning.root.index + oldTurn.notePool[oldTurn.currentNoteObj.string] + userInput.fret)
  const inputNoteCorrect = (inputNote === newTurn.currentNoteObj.note)

  newTurn.currentNoteObj.tries.push({
    fret: userInput.fret,
    string: newTurn.currentNoteObj.string,
    note: inputNote,
    noteIndex: inputNoteCorrect,
    time: newTurn.time.current,
    timeTicked: newTurn.time.ticked
  })

  if (inputNoteCorrect) {
    if (oldTurn.status === 'start') newTurn.status = 'inProgress'

    newTurn.scoreAdded = Math.pow((newTurn.gameLevel + 1), 2.2) * 120 + getRandomInt(500)
    newTurn.currentNoteObj.status = 'win'
    newTurn.turns.push(newTurn.currentNoteObj)
    if (newTurn.turns.filter(el => el.status === 'win').length >= getLevel(newTurn.gameLevel).goal) {
      newTurn = levelUp(newTurn)
    }
    newTurn.currentNoteObj = makeNewNoteObj(getLevel(gameLevel), newTurn.currentNoteObj)
    newTurn.score += newTurn.scoreAdded
  }

  return newTurn
}

const arpeggiatorTurn = (
  currentSetup,
  gameSettings,
  oldTurn,
  userInput = 'undefined',
  gameLevel = 0
  ) => {
  const notePool = makeNotePool(currentSetup.tuning)
  const getLevel = (level, newTurnLength) => {
    const gameLevelNew = Math.min(level, arpeggiatorSettings.levels.length - 1)
    const gameLevelObjNew = arpeggiatorSettings.levels[gameLevelNew]

    return Object.assign({}, gameLevelObjNew, { noteLife: newTurnLength > 2 ? Math.min(gameLevelObjNew.noteLife + (3.5 * gameLevelObjNew.noteLife * (newTurnLength - 2)), 30) : gameLevelObjNew.noteLife }, { totalTime: newTurnLength > 2 ? Math.min(gameLevelObjNew.totalTime + (0.7 * gameLevelObjNew.totalTime * (newTurnLength - 2)), 30) : gameLevelObjNew.totalTime })
  }
  const levelUp = (turnObj) => {
    const gameLevelNew = turnObj.gameLevel + 1
    return Object.assign({}, turnObj, {
      time: {
        allowed: turnObj.time.allowed + getLevel(gameLevelNew).totalTime,
        current: Math.min(turnObj.time.current + getLevel(gameLevelNew).totalTime, 45),
        ticked: turnObj.time.ticked,
        added: getLevel(gameLevelNew).totalTime
      },
      allTurns: turnObj.allTurns.concat(turnObj.turns),
      turns: [],
      gameLevel: gameLevelNew,
      gameLevelObj: getLevel(gameLevelNew)
    })
  }
  const makeNewTurn = () => {
    return {
      time: {
        allowed: arpeggiatorSettings.levels[gameLevel].totalTime,
        current: arpeggiatorSettings.levels[gameLevel].totalTime,
        ticked: 0,
        added: 0
      },
      string: getRandomInt(notePool.length),
      notePool,
      allTurns: [],
      turns: [],
      currentTurn: [],
      gameLevel,
      gameLevelObj: arpeggiatorSettings.levels[gameLevel],
      score: 0,
      scoreAdded: 0,
      status: 'start',
      noteArray: makeNoteArray(gameSettings.chord) // init the noteArray from currentSettings
    }
  }

  if (((oldTurn.status === 'start' || oldTurn.status === 'win') && userInput === 'undefined') || (oldTurn.status !== 'win' && userInput === 'forward')) return oldTurn
  let newTurn
  if (oldTurn === 'undefined' || userInput === 'reset') {
    newTurn = makeNewTurn()
    return newTurn
  }

  newTurn = Object.assign({}, oldTurn)
  newTurn.scoreAdded = 0
  if (oldTurn.status === 'win' && userInput === 'forward') {

    newTurn.turns.push(newTurn.currentTurn)
    newTurn.currentTurn = []
    if (newTurn.turns.length >= getLevel(newTurn.gameLevel).goal) {
      newTurn = levelUp(newTurn)
    }
    newTurn.status = 'inProgress'
    newTurn.noteArray = newTurn.noteArray.map(el => {
      return {
        name: el.name,
        time: -1,
        index: el.index
      }
    })
    return newTurn
  }
  // console.log(newTurn)
  if (userInput === 'undefined') {
    newTurn.time.current--
    newTurn.time.ticked++
    if (newTurn.time.current <= 0) {
      newTurn.time.current = 0
      newTurn.status = 'loss'
    }
    newTurn.noteArray = newTurn.noteArray.map(el => {
      return {
        name: el.name,
        time: el.time - 1,
        index: el.index
      }
    })
    return newTurn
  }

  const inputNoteIndexFull = currentSetup.tuning.root.index + oldTurn.notePool[oldTurn.string] + userInput.fret
  const inputNote = getNoteName(inputNoteIndexFull)
  const inputNoteIndex = newTurn.noteArray.findIndex(el => el.name === inputNote)
  // add user input to the currentTurn array
  newTurn.currentTurn.push({
    fret: userInput.fret,
    string: newTurn.string,
    note: inputNote,
    noteIndex: inputNoteIndex,
    time: newTurn.time.current,
    timeTicked: newTurn.time.ticked
  })
  /* check for correct user turn */
  if (inputNoteIndex > -1 /* && newTurn.noteArray[inputNoteIndex].time < 0 */) {
    /* correct userTurn */

    /* correct userTurn */
    newTurn.noteArray[inputNoteIndex].time = getLevel(newTurn.gameLevel).noteLife
    newTurn.noteArray[inputNoteIndex].index = inputNoteIndexFull
    newTurn.string = getRandomInt(notePool.length, newTurn.string)

    /*
     check for all array being positive
     -> would mean chord is completed
    */
    const correctNotes = newTurn.noteArray.filter(el => el.time > 0).length

    /* correct user turn score */
    newTurn.scoreAdded = Math.floor(Math.pow(newTurn.gameLevel + 1, 1.5) * Math.pow(correctNotes, 2) * 100 + getRandomInt(50 * newTurn.gameLevel))

    if (oldTurn.status === 'start') newTurn.status = 'inProgress'

    if (correctNotes === newTurn.noteArray.length) {
      newTurn.scoreAdded += Math.floor(Math.pow(newTurn.gameLevel + 1, 1.5) * Math.pow(correctNotes, 3) * 400 + getRandomInt(150 * newTurn.gameLevel))
      if (newTurn.currentTurn.length === correctNotes) {
        newTurn.scoreAdded = newTurn.scoreAdded * (correctNotes - 0.7)
      }
      newTurn.status = 'win'
    }

    newTurn.score += newTurn.scoreAdded
  }
  // fall through to returning a turn
  return newTurn
}

const defaultScale = library.scales.rootSpecific.G[0]
const defaultScaleFretboard = library.scales.all
const defaultScaleRootIndex = defaultScale.rootIndex || 0
const defaultChordType = defaultScale.chordArray[0]
const defaultChordRoot = defaultChordType.scaleRoots[0]

const makeArpeggiatorArchive = () => {
  const makeOctaveArray = (acc, el) => {
    acc[el] = []
    return acc
  }
  return library.chords.reduce((acc, el) => {
    acc[el.name] = library.octave.reduce(makeOctaveArray, {})
    return acc
  }, {})
}

const makeArpeggiatorStatsArchive = () => {
  const makeOctaveArray = (acc, el) => {
    acc[el] = {
      totalCorrect: 0,
      totalAttempts: 0,
      totalComplete: 0,
      highScore: 0,
      highLevel: 0,
      totalScore: 0,
      other: {}
    }
    return acc
  }
  return Object.assign(
      {},
      library.chords.reduce((acc, el) => {
        acc[el.name] = Object.assign(
          {},
          library.octave.reduce(makeOctaveArray, {}),
          {
            totalCorrect: 0,
            totalAttempts: 0,
            totalComplete: 0,
            highScore: 0,
            highLevel: 0,
            totalScore: 0,
            Player: 0,
            Perfectionist: 0,
            Shooter: 0,
            Equalist: 0,
            Overachiever: 0,
            totalAchievements: 0,
            other: {}
          }
        )
        return acc
      }, {}),
    {
      totalCorrect: 0,
      totalAttempts: 0,
      totalComplete: 0,
      highScore: 0,
      highLevel: 0,
      totalScore: 0,
      totalAchievements: 0,
      other: {}
    }
  )
}

const adjustStatObj = (statObj, statNew) => {
  return Object.assign({}, statObj, {
    highScore: statObj.highScore > statNew.score ? statObj.highScore : statNew.score,
    highLevel: statObj.highLevel > statNew.gameLevel ? statObj.highLevel : statNew.gameLevel,
    totalCorrect: statObj.totalCorrect + statNew.totalCorrect,
    totalAttempts: statObj.totalAttempts + statNew.totalAttempts,
    totalComplete: statObj.totalComplete + statNew.totalComplete,
    totalScore: statObj.totalScore + statNew.score
  })
}

const makeFlatArchive = (typeArchive) => library.octave.reduce((accc, ell) => [...accc, ...typeArchive[ell]], [])
export const compareAchievements = (achievementsA, achievementsB) => {
  const Player = Math.max(achievementsA.Player, achievementsB.Player)
  const Perfectionist = Math.max(achievementsA.Perfectionist, achievementsB.Perfectionist)
  const Shooter = Math.max(achievementsA.Shooter, achievementsB.Shooter)
  const Equalist = Math.max(achievementsA.Equalist, achievementsB.Equalist)
  const Overachiever = Math.max(achievementsA.Overachiever, achievementsB.Overachiever)
  return {
    Player,
    Perfectionist,
    Shooter,
    Equalist,
    Overachiever,
    totalAchievements: Player + Perfectionist + Shooter + Equalist + Overachiever
  }
}
export const diffAchievements = (achievementsA, achievementsB) => {
  const Player = achievementsB.Player - achievementsA.Player
  const Perfectionist = achievementsB.Perfectionist - achievementsA.Perfectionist
  const Shooter = achievementsB.Shooter - achievementsA.Shooter
  const Equalist = achievementsB.Equalist - achievementsA.Equalist
  const Overachiever = achievementsB.Overachiever - achievementsA.Overachiever
  return {
    Player,
    Perfectionist,
    Shooter,
    Equalist,
    Overachiever,
    totalAchievements: Player + Perfectionist + Shooter + Equalist + Overachiever
  }
}


export const checkForEqualist = (typeArchive) => {
  const flatArchive = makeFlatArchive(typeArchive)
    .sort((elA, elB) => elA.gameLevel > elB.gameLevel)
    .reverse()
    .filter((ell, index, self) => self.findIndex(elll => elll.root === ell.root) === index)
  const equalistLevels = library.achievements.arpeggiator.find(ell => ell.trait === 'Equalist').levels
  const maxEqualist = equalistLevels.reduce((accc, ell, index) => {
    const tempAmount = flatArchive.filter(elll => elll.gameLevel >= ell.req.gameLevel)
    if (tempAmount.length >= ell.req.different) {
      return index + 1
    }
    return accc
  }, 0)
  return maxEqualist
}

export const makeAchievements = (typeArchive) => {
  const flatArchive = makeFlatArchive(typeArchive)

  const newObj = {}

  // testForPlayer
  const sortedByLevel = flatArchive.sort((elA, elB) => elA.gameLevel > elB.gameLevel)
  const playerLevels = library.achievements.arpeggiator.find(ell => ell.trait === 'Player').levels
  const maxPlayer = playerLevels.reduce((accc, ell, index) => {
    const tempIndex = sortedByLevel.findIndex(elll => elll.gameLevel >= ell.req.gameLevel)
    //console.log(`${tempIndex} vs ${sortedByLevel.length - 1}`)
    if (tempIndex >= 0 && sortedByLevel.length - 1 - tempIndex >= ell.req.count - 1) {
      return index + 1
    }
    return accc
  }, 0)
  newObj.Player = maxPlayer
  // testForShooter
  const shooterLevels = library.achievements.arpeggiator.find(ell => ell.trait === 'Shooter').levels
  const maxShooter = shooterLevels.reduce((accc, ell, index) => {
    if (sortedByLevel.find(elll => elll.gameLevel >= ell.req.gameLevel && elll.totalCorrect / elll.totalAttempts * 100 > ell.req.accuracy)) {
      return index + 1
    }
    return accc
  }, 0)

  newObj.Shooter = maxShooter
  // testForPerfectionist ---> make them all with accuracy- filter 100% accuracy
  
  const perfectionistLevels = library.achievements.arpeggiator.find(ell => ell.trait === 'Perfectionist').levels
  const maxPerfectionist = perfectionistLevels.reduce((accc, ell, index) => {
    if (sortedByLevel.find(elll => elll.gameLevel >= ell.req.gameLevel && elll.totalAttempts === elll.totalCorrect)) {
      return index + 1
    }
    return accc
  }, 0)
  newObj.Perfectionist = maxPerfectionist
  // testForOverachiever ---> just find high score
  const highScore = flatArchive.reduce((accc, ell) => accc > ell.score ? accc : ell.score, 0)
  const overachieverLevels = library.achievements.arpeggiator.find(ell => ell.trait === 'Overachiever').levels
  const maxOverachiever = overachieverLevels.reduce((accc, ell, index) => ell.req.score <= highScore ? index + 1 : accc, 0)
  newObj.Overachiever = maxOverachiever
  // testForEqualist ---- NEED TO ADJUST, CURRENTLY NOT CHECKING FOR DIFFERENT
  newObj.Equalist = checkForEqualist(typeArchive)
  // total
  newObj.totalAchievements = maxOverachiever + maxPlayer + newObj.Equalist + maxPerfectionist + maxShooter

  return newObj
  /*
    output should be

    {
      5th: {
        player: 0,
        perfectionist: 5,
        shooter: 2,
        all: 7
      },
      Major: {
        ...
      }
    }

    return {
      Player: 0,
      Perfectionist: 0,
      Shooter: 0,
      Equalist: 0,
      Overachiever: 0,
      totalAchievements: 0
    }
   */
}

export const updateStatArchive = (statArchive, currentChord, currentGame, archive) => {
  /*
    need update on 3 levels
    statArchive
    statArchive[currentChord.type]
    statArchive[currentChord.type][currentChord.root]
   */
  const nameObj = {}
  nameObj[currentChord.root] = adjustStatObj(statArchive[currentChord.name][currentChord.root], currentGame)
  const mainObj = {}
  const achievements = makeAchievements(archive[currentChord.name], currentGame)
  mainObj[currentChord.name] = Object.assign(adjustStatObj(statArchive[currentChord.name], currentGame), achievements, nameObj)
  

  return Object.assign(adjustStatObj(statArchive, currentGame), mainObj, { totalAchievements: statArchive.totalAchievements - statArchive[currentChord.name].totalAchievements + Math.max(statArchive[currentChord.name].totalAchievements, achievements.totalAchievements) })
}
export const gameObjects = [
  {
    name: 'Arpeggiator Of Death',
    id: 'arpeggiator',
    turn: arpeggiatorTurn,
    settings: arpeggiatorSettings,
    setupComponent: 'ArpeggiatorSetup',
    gameComponent: 'ArpeggiatorPlay',
    gamePauseComponent: 'ArpeggiatorPause',
    gameEndComponent: 'ArpeggiatorEnd',
    currentSetup: {},
    archive: makeArpeggiatorArchive(),
    statsArchive: makeArpeggiatorStatsArchive(),
    defaultSetup: { chord: getChordObject(defaultChordRoot, defaultChordType, defaultScaleRootIndex), scale: defaultScale }
  },
  {
    name: 'Fretboard',
    id: 'fretboard',
    turn: fretboardTurn,
    settings: fretboardSettings,
    setupComponent: 'FretboardSetup',
    gameComponent: 'FretboardPlay',
    gamePauseComponent: 'FretboardPause',
    gameEndComponent: 'FretboardEnd',
    currentSetup: {},
    archive: [],
    statsArchive: { totalScore: 0 },
    defaultSetup: { selected: [], selectable: getScaleNotes(defaultScaleFretboard), scale: defaultScaleFretboard }
  }
]
