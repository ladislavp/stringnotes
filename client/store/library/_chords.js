export const chords = [
  /*
  5th
  C,G
  C,C#,D,D#,E,F,F#,G,A,A#
  0,1 ,2,3 ,4,5,6 ,7,
  */
  {
    name: '5th',
    notes: [0, 7],
    suffix: '5',
    difficulty: 'easy'
  },
  /*
  MAJOR
  C, E, G
  */
  {
    name: 'Major',
    notes: [0, 4, 7],
    suffix: '',
    difficulty: 'medium'
  },
  /*
  MINOR
  C, D#, G
  */
  {
    name: 'Minor',
    notes: [0, 3, 7],
    suffix: 'm',
    difficulty: 'medium'
  },

  /*
  suspended 2nd
  C, D, G
   */
  {
    name: 'Suspended 2nd',
    notes: [0, 2, 7],
    suffix: 'sus2 ',
    difficulty: 'medium'
  },

  /*
  suspended 4th
  C, D, G
   */
  {
    name: 'Suspended 4th',
    notes: [0, 5, 7],
    suffix: 'sus4',
    difficulty: 'medium'
  },
  /*
  7th
  C, E, G, A#
   */
  {
    name: 'Dominant 7th',
    notes: [0, 4, 7, 10],
    suffix: '7',
    difficulty: 'medium'
  },

  /*
  Major 7th
  C, E, G, B
   */
  {
    name: 'Major 7th',
    notes: [0, 4, 7, 11],
    suffix: 'M7',
    difficulty: 'medium'
  },

  /*
  Minor 7th
  C, D#, G, A#
   */
  {
    name: 'Minor 7th',
    notes: [0, 3, 7, 10],
    suffix: 'm7',
    difficulty: 'medium'
  },

  /*
  Major add9
   */
  {
    name: 'Major add9',
    notes: [0, 4, 7, 14],
    suffix: 'add9',
    difficulty: 'medium'
  },

  /*
  Minor Major 7th
  C, D#, G, B
   */
  {
    name: 'Minor Major 7th',
    notes: [0, 3, 7, 11],
    suffix: 'mM7',
    difficulty: 'medium'
  },

  /*
  Diminished triad
  E,G,A#
   */
  {
    name: 'Diminished triad',
    notes: [0, 3, 6],
    suffix: 'dim',
    difficulty: 'medium'
  },
  /*
  Major diminished 5th
  E,G#,A#
  */
  {
    name: 'Major diminished 5th',
    notes: [0, 4, 6],
    suffix: '-5',
    difficulty: 'medium'
  },

  /*
  Major 6th
  C, E, G, A
   */
  {
    name: 'Major 6th',
    notes: [0, 4, 7, 9],
    suffix: 'M6',
    difficulty: 'medium'
  },

  /*
  Minor 6th
  C, E, G, A
   */
  {
    name: 'Minor 6th',
    notes: [0, 3, 7, 9],
    suffix: 'm6',
    difficulty: 'medium'
  },

  /*
  Augmented 7th
  C, E, G#, A#
   */
  {
    name: 'Augmented 7th',
    notes: [0, 4, 8, 10],
    suffix: 'maug7',
    difficulty: 'medium'
  },
  /*
  Diminished 7th
  E,G,A#
   */
  {
    name: 'Diminished 7th',
    notes: [0, 3, 6, 9],
    suffix: 'dim7',
    difficulty: 'medium'
  },

  /*
  Augmented-major 7th
  C, E, G#, B
   */
  {
    name: 'Augmented-major 7th',
    notes: [0, 4, 8, 11],
    suffix: 'aug7',
    difficulty: 'medium'
  },
  /*
  major 7th suspended 2nd
  C, D, G, B
   */
  {
    name: 'Major 7th suspended 2nd',
    notes: [0, 2, 7, 11],
    suffix: 'M7sus2',
    difficulty: 'medium'
  },
  /*
  major 7th suspended 4th
  C, F, G, B
   */
  {
    name: 'Major 7th suspended 4th',
    notes: [0, 5, 7, 11],
    suffix: 'M7sus4',
    difficulty: 'medium'
  },

  /*
  Major 7th Flat 5
  */
  {
    name: 'Major 7th flat 5',
    notes: [0, 4, 6, 11],
    suffix: 'M7b5',
    difficulty: 'medium'
  },
  /*
  Major 7th Sharp 5
  */
  {
    name: 'Major 7th sharp 5',
    notes: [0, 4, 8, 11],
    suffix: 'M7#5',
    difficulty: 'medium'
  },

  /*
  Suspended 2nd suspended 4th
  */
  {
    name: 'Suspended 2nd suspended 4th',
    notes: [0, 2, 5, 7],
    suffix: 'sus2sus4',
    difficulty: 'medium'
  },

  /*
  Dominant ninth
  */
  {
    name: 'Dominant 9th',
    notes: [0, 4, 7, 10, 14],
    suffix: '9',
    difficulty: 'hard'
  },
  /*
  Major ninth
  */
  {
    name: 'Major 9th',
    notes: [0, 4, 7, 11, 14],
    suffix: 'M9',
    difficulty: 'hard'
  },
  /*
  Minor ninth
  */
  {
    name: 'Minor 9th',
    notes: [0, 3, 7, 10, 14],
    suffix: 'm9',
    difficulty: 'hard'
  },
  /*
  Dominant 7th flat 9
   */
  {
    name: 'Dominant 7th flat 9',
    notes: [0, 4, 7, 10, 13],
    suffix: '7b9',
    difficulty: 'hard'
  },

  /*
  Dominant 7th sharp 9
   */
  {
    name: 'Dominant 7th sharp 9',
    notes: [0, 4, 7, 10, 15],
    suffix: '7#9',
    difficulty: 'hard'
  },
  /*
  Major 6th add 9
   */
  {
    name: 'Major 6 add9',
    notes: [0, 4, 7, 9, 14],
    suffix: '6add9',
    difficulty: 'hard'
  },

  /*
  Minor major  9
  */
  {
    name: 'Minor major 9th',
    notes: [0, 3, 7, 11, 14],
    suffix: 'mm9',
    difficulty: 'hard'
  },
  /*
  Dominant  9th flat 5
  */
  {
    name: 'Dominant 9th flat 5',
    notes: [0, 4, 6, 10, 14],
    suffix: '9b5',
    difficulty: 'hard'
  },
  /*
  Dominant  9th sharp 5
  */
  {
    name: 'Dominant 9th sharp 5',
    notes: [0, 4, 8, 10, 14],
    suffix: '9#5',
    difficulty: 'hard'
  },
  /*
  Dominant eleventh
  */
  {
    name: 'Dominant 11th',
    notes: [0, 4, 7, 10, 14, 17],
    suffix: '11',
    difficulty: 'hard'
  },
  /*
  Minor eleventh
  */
  {
    name: 'Minor 11th',
    notes: [0, 3, 7, 10, 14, 17],
    suffix: 'm11',
    difficulty: 'hard'
  },
  /*
  Dominant eleventh flat 9
  */
  {
    name: 'Dominant 11th flat 9',
    notes: [0, 4, 7, 10, 13, 17],
    suffix: '11b9',
    difficulty: 'hard'
  },
  /*
  Major 9th sharp 11
  */
  {
    name: 'Major 9th sharp 11',
    notes: [0, 4, 7, 11, 14, 18],
    suffix: '11b9',
    difficulty: 'hard'
  },
  /*
  Dominant 13th
  */
  {
    name: 'Dominant 13th',
    notes: [0, 4, 7, 10, 14, 21],
    suffix: '13',
    difficulty: 'hard'
  },
  /*
  Major 13th
  */
  {
    name: 'Major 13th',
    notes: [0, 4, 7, 11, 14, 21],
    suffix: 'M13',
    difficulty: 'hard'
  },
  /*
  Minor 13th
  */
  {
    name: 'Minor 13th',
    notes: [0, 3, 7, 10, 14, 21],
    suffix: 'm13',
    difficulty: 'hard'
  },
  /*
  Dominant  7th flat 5 flat 9
  */
  {
    name: 'Dominant 7th flat 5 flat 9',
    notes: [0, 4, 6, 10, 13],
    suffix: '7(b5b9)',
    difficulty: 'hard'
  },
  /*
  Dominant  7th flat 5 sharp 9
  */
  {
    name: 'Dominant 7th flat 5 sharp 9',
    notes: [0, 4, 6, 10, 15],
    suffix: '7(b5#9)',
    difficulty: 'hard'
  },
  /*
  Dominant  7th sharp 5 flat 9
  */
  {
    name: 'Dominant 7th sharp 5 flat 9',
    notes: [0, 4, 8, 10, 13],
    suffix: '7(#5b9)',
    difficulty: 'hard'
  },
  /*
  Dominant  7th sharp 5 flat 9
  */
  {
    name: 'Dominant 7th sharp 5 sharp 9',
    notes: [0, 4, 8, 10, 15],
    suffix: '7(#5#9)',
    difficulty: 'hard'
  },
  /*
  Dominant 13th sharp 11
  */
  {
    name: 'Dominant 13th sharp 11',
    notes: [0, 4, 7, 10, 14, 18, 21],
    suffix: '13#11',
    difficulty: 'hard'
  },
  /*
  Major 13th sharp 11
  */
  {
    name: 'Major 13th sharp 11',
    notes: [0, 4, 7, 11, 14, 18, 21],
    suffix: 'M13#11',
    difficulty: 'hard'
  }
]
