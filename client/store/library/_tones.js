export let tones = []
export const octave = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

for (let i = 0; i <= 8; i++) {
  tones = [...tones, ...octave.map((val, c) => {
    return {
      value: `${val}${i}`,
      note: val,
      octave: i,
      index: i * octave.length + c
    }
  })]
}
