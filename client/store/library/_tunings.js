export const tunings = {
  guitar: [
    {
      root: 'E2',
      name: 'standard',
      title: 'E standard',
      strings: [0, 5, 10, 15, 19, 24]
    },
    {
      root: 'D#2',
      name: 'standard',
      title: 'Eb standard',
      strings: [0, 5, 10, 15, 19, 24]
    },
    {
      root: 'D2',
      name: 'drop',
      title: 'Drop D',
      strings: [0, 7, 12, 17, 21, 26]
    },
    {
      root: 'C2',
      name: 'drop',
      title: 'Drop C',
      strings: [0, 7, 12, 17, 21, 26]
    },
    {
      root: 'C#2',
      name: 'drop',
      title: 'Drop C#',
      strings: [0, 7, 12, 17, 21, 26]
    },
    {
      root: 'E2',
      name: 'open',
      title: 'Open E',
      strings: [0, 7, 12, 16, 19, 24]
    },
    {
      root: 'D2',
      name: 'open',
      title: 'Open D',
      strings: [0, 7, 12, 16, 19, 24]
    },
    {
      root: 'E2',
      name: 'open',
      title: 'Open A',
      strings: [0, 5, 12, 17, 21, 24]
    },
    {
      root: 'D2',
      name: 'open',
      title: 'Open G',
      strings: [0, 5, 12, 17, 21, 24]
    },
    {
      root: 'C2',
      name: 'open',
      title: 'Open C',
      strings: [0, 7, 12, 16, 19, 24]
    }
  ],
  bass: [
    {
      root: 'E1',
      name: 'standard',
      title: 'E standard',
      strings: [0, 5, 10, 15]
    },
    {
      root: 'D1',
      name: 'drop',
      title: 'Drop D',
      strings: [0, 7, 12, 17]
    },
    {
      root: 'C1',
      name: 'drop',
      title: 'Drop C',
      strings: [0, 7, 12, 17, 21, 26]
    },
    {
      root: 'D#1',
      name: 'standard',
      title: 'Eb standard',
      strings: [0, 5, 10, 15]
    }
  ],
  mandolin: [
    {
      root: 'G3',
      name: 'standard',
      title: 'Standard',
      strings: [0, 7, 14, 21]
    }
  ],
  violin: [
    {
      root: 'G3',
      name: 'standard',
      title: 'Standard',
      strings: [0, 7, 14, 21]
    }
  ],
  ukulele: [
    {
      root: 'C4',
      rootName: 'G',
      name: 'standard',
      title: 'Tenor High G tuning',
      strings: [7, 0, 4, 9]
    },
    {
      root: 'G3',
      rootName: 'G',
      name: 'standard',
      title: 'Tenor Low G tuning',
      strings: [0, 5, 9, 14]
    },
    {
      root: 'G3',
      rootName: 'G',
      name: 'standard',
      title: 'Baritone tuning',
      strings: [7, 0, 4, 9]
    }
  ]
}
