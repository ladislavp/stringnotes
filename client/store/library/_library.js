import { scales } from './_scales'
import { tones, octave } from './_tones'
import { tunings } from './_tunings'
import { chords } from './_chords'
import { ac, makeSound } from './_audioContext'
import { didYouKnow } from './_didYouKnow'
import { intervals } from './_intervals'
import { instruments } from './_instruments'
import { achievements, achievementTypes } from './_achievements'
import { constants } from './_constants'
import { characterLevels } from './_character'

export const library = {
  scales,
  tones,
  octave,
  tunings,
  chords,
  ac,
  makeSound,
  didYouKnow,
  intervals,
  instruments,
  achievements,
  achievementTypes,
  constants,
  characterLevels
}
