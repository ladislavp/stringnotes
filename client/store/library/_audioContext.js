import { octave, tones } from './_tones.js'
import { tunings } from './_tunings'
export let ac
const AudioContext = window.AudioContext // Default
    || window.webkitAudioContext // Safari and old versions of Chrome
    || false;

if (AudioContext) {
  // Do whatever you want using the Web Audio API
  ac = new AudioContext;
  // ...
} else {
  // Web Audio API is not supported
  // Alert the user
  console.log('Sorry, but the Web Audio API is not supported by your browser. Please,' +
    ' consider upgrading to the latest version or downloading Google Chrome or Mozilla Firefox');
}

// start with E4 as a min root
// add octave numbers from that
export const makeSound = (noteArray, instrument, volume, tuning, sustain = 1.4) => {
  const tuningStart = tones.findIndex(el => el.value === tuning.root.value)

  const arrayStart = tones.findIndex((el, index) => el.note === noteArray[0] && index >= tuningStart)

  if (!instrument) return 0

  noteArray.map((el, index) => {
    if (index === 0) {
      instrument.play(
        tones[arrayStart].value,
        0, { gain: Math.floor(volume / 100 * 8.5), sustain }
      )
    } else {
      instrument.play(
        tones.find((ell, i) => ell.note === el && i > tuningStart).value,
        0, { gain: Math.floor(volume / 100 * 8.5), sustain }
      )
    }
    return 1
  })
}

// E2,F2...B2,C3,D3
// first octave.findIndex(el === noteArray[0]) < 4 -> be 3 else be 2
// get octave.findIndex(el === noteArray[0]) + octave.findIndex(el === noteArray[index])

