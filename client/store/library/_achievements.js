export const achievements = {
  arpeggiator: [
    {
      trait: 'Player',
      levels: [
        {
          req: {
            gameLevel: 1,
            count: 1
          },
          text: "Play a game"
        },
        {
          req: {
            gameLevel: 3,
            count: 3
          },
          text: "Reach level 3 on 3 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 5
          },
          text: "Reach level 3 on 5 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 7
          },
          text: "Reach level 3 on 7 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 10
          },
          text: "Reach level 3 on 10 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 15
          },
          text: "Reach level 3 on 15 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 20
          },
          text: "Reach level 3 on 20 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 30
          },
          text: "Reach level 3 on 30 games of this chord type"
        },
        {
          req: {
            gameLevel: 3,
            count: 40
          },
          text: "Reach level 3 on 40 games of this chord type"
        }
      ]
    },
    {
      trait: 'Shooter',
      levels: [
        {
          req: {
            gameLevel: 3,
            accuracy: 80
          },
          text: "Finish game on level 3 or above with above 80% accuracy"
        },
        {
          req: {
            gameLevel: 5,
            accuracy: 82
          },
          text: "Finish game on level 5 or above with above 82% accuracy"
        },
        {
          req: {
            gameLevel: 7,
            accuracy: 85
          },
          text: "Finish game on level 7 or above with above 85% accuracy"
        },
        {
          req: {
            gameLevel: 10,
            accuracy: 87
          },
          text: "Finish game on level 10 or above with above 90% accuracy"
        },
        {
          req: {
            gameLevel: 15,
            accuracy: 90
          },
          text: "Finish game on level 15 or above  with above 90% accuracy"
        },
        {
          req: {
            gameLevel: 17,
            accuracy: 90
          },
          text: "Finish game on level 17 or above  with above 90% accuracy"
        },
        {
          req: {
            gameLevel: 19,
            accuracy: 90
          },
          text: "Finish game on level 19 or above  with above 90% accuracy"
        },
        {
          req: {
            gameLevel: 21,
            accuracy: 90
          },
          text: "Finish game on level 19 or above  with above 90% accuracy"
        }
      ]
    },
    {
      trait: 'Perfectionist',
      levels: [
        {
          req: {
            gameLevel: 2,
            accuracy: 100
          },
          text: "Finish game on level 2 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 3,
            accuracy: 100
          },
          text: "Finish game on level 3 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 4,
            accuracy: 100
          },
          text: "Finish game on level 4 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 5,
            accuracy: 100
          },
          text: "Finish game on level 5 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 6,
            accuracy: 100
          },
          text: "Finish game on level 6 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 7,
            accuracy: 100
          },
          text: "Finish game on level 7 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 8,
            accuracy: 100
          },
          text: "Finish game on level 8 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 9,
            accuracy: 100
          },
          text: "Finish game on level 8 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 9,
            accuracy: 100
          },
          text: "Finish game on level 9 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 10,
            accuracy: 100
          },
          text: "Finish game on level 10 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 12,
            accuracy: 100
          },
          text: "Finish game on level 12 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 13,
            accuracy: 100
          },
          text: "Finish game on level 13 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 14,
            accuracy: 100
          },
          text: "Finish game on level 14 or above with perfect accuracy"
        },
        {
          req: {
            gameLevel: 15,
            accuracy: 100
          },
          text: "Finish game on level 15 or above with perfect accuracy"
        }
      ]
    },
    {
      trait: 'Overachiever',
      levels: [
        {
          req: {
            score: 50000
          },
          text: "Highest score above 50000pts."
        },
        {
          req: {
            score: 150000
          },
          text: "Highest score above 150000pts."
        },
        {
          req: {
            score: 600000
          },
          text: "Highest score above 600000pts."
        },
        {
          req: {
            score: 1000000
          },
          text: "Highest score above 1000000pts."
        },
        {
          req: {
            score: 1500000
          },
          text: "Highest score above 1500000pts."
        },
        {
          req: {
            score: 2200000
          },
          text: "Highest score above 2200000pts."
        },
        {
          req: {
            score: 3000000
          },
          text: "Highest score above 3100000pts."
        },
        {
          req: {
            score: 4500000
          },
          text: "Highest score above 4500000pts."
        },
        {
          req: {
            score: 6000000
          },
          text: "Highest score above 6000000pts."
        },
      ]
    },
    {
      trait: 'Equalist',
      levels: [
        {
          req: {
            different: 2,
            gameLevel: 2
          },
          text: "Reach level 2 on 2 different chord roots"
        },
        {
          req: {
            different: 3,
            gameLevel: 3
          },
          text: "Reach level 3 on 3 different chord roots"
        },
        {
          req: {
            different: 4,
            gameLevel: 4
          },
          text: "Reach level 4 on 4 different chord roots"
        },
        {
          req: {
            different: 5,
            gameLevel: 5
          },
          text: "Reach level 5 on 5 different chord roots"
        },
        {
          req: {
            different: 6,
            gameLevel: 6
          },
          text: "Reach level 6 on 6 different chord roots"
        },
        {
          req: {
            different: 7,
            gameLevel: 7
          },
          text: "Reach level 7 on 7 different chord roots"
        },
        {
          req: {
            different: 8,
            gameLevel: 8
          },
          text: "Reach level 8 on 8 different chord roots"
        }
      ]
    }
  ]
}

export const achievementTypes = {
  arpeggiator: [
    'Player', 'Shooter', 'Perfectionist', 'Equalist', 'Overachiever'
  ]
}
// with time to spare (extra time when level up), streaker?
