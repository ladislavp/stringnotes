export const arpeggiatorSettings = {
  levels: [
    {
      totalTime: 45,
      noteLife: 30,
      goal: 4
    },
    {
      totalTime: 20,
      noteLife: 20,
      goal: 3
    },
    {
      totalTime: 20,
      noteLife: 10,
      goal: 3
    },
    {
      totalTime: 15,
      noteLife: 7,
      goal: 3
    },
    {
      totalTime: 15,
      noteLife: 6,
      goal: 3
    },
    {
      totalTime: 15,
      noteLife: 5,
      goal: 4
    },
    {
      totalTime: 15,
      noteLife: 3,
      goal: 4
    },
    {
      totalTime: 15,
      noteLife: 3,
      goal: 4
    },
    {
      totalTime: 15,
      noteLife: 3,
      goal: 5
    },
    {
      totalTime: 15,
      noteLife: 3,
      goal: 5
    },
    {
      totalTime: 15,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 14,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 13,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 12,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 12,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 12,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 11,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 10,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 10,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 10,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 10,
      noteLife: 2,
      goal: 5
    },
    {
      totalTime: 10,
      noteLife: 2,
      goal: 5
    }
  ]
}