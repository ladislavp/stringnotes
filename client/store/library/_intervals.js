export const intervals = [
  [
    {
      name: 'Root note',
      short: 'R',
      type: 'Root',
      order: 0
    },
    {
      name: 'Perfect unison',
      short: 'P1',
      type: 'Perfect',
      group: 'standard',
      order: 1
    },
    {
      name: 'Diminished second',
      short: 'd2',
      type: 'Diminished',
      group: 'diminished',
      order: 2
    }
  ],
  [
    {
      name: 'Minor second',
      short: 'm2',
      type: 'Minor',
      group: 'standard',
      order: 2
    },
    {
      name: 'Augmented unison',
      short: 'A1',
      type: 'Augmented',
      group: 'augmented',
      order: 1
    },
    {
      name: 'Minor ninth',
      short: 'm9',
      type: 'Minor',
      group: 'standard',
      order: 9
    }
  ],
  [
    {
      name: 'Major second',
      short: 'M2',
      type: 'Major',
      group: 'standard',
      order: 2
    },
    {
      name: 'Diminished third',
      short: 'd3',
      type: 'Diminished',
      group: 'diminished',
      order: 3
    },
    {
      name: 'Major ninth',
      short: 'M9',
      type: 'Major',
      group: 'standard',
      order: 9
    }
  ],
  [
    {
      name: 'Minor third',
      short: 'm3',
      type: 'Minor',
      group: 'standard',
      order: 3
    },
    {
      name: 'Augmented second',
      short: 'A2',
      type: 'Augmented',
      group: 'augmented',
      order: 2
    },
    {
      name: 'Augmented ninth',
      short: 'A9',
      type: 'Augmented',
      group: 'augmented',
      order: 9
    }
  ],
  [
    {
      name: 'Major third',
      short: 'M3',
      type: 'Major',
      group: 'standard',
      order: 3
    },
    {
      name: 'Diminished fourth',
      short: 'd4',
      type: 'Diminished',
      group: 'diminished',
      order: 4
    }
  ],
  [
    {
      name: 'Perfect fourth',
      short: 'P4',
      type: 'Perfect',
      group: 'standard',
      order: 4
    },
    {
      name: ' Augmented third',
      short: 'A3',
      type: 'Augmented',
      group: 'augmented',
      order: 3
    }
  ],
  [
    {
      name: 'Diminished fifth',
      short: 'd5',
      type: 'Diminished',
      group: 'diminished',
      order: 5
    },
    {
      name: 'Augmented fourth',
      short: 'A4',
      type: 'Augmented',
      group: 'augmented',
      order: 4
    }
  ],
  [
    {
      name: 'Perfect fifth',
      short: 'P5',
      type: 'Perfect',
      group: 'standard',
      order: 5
    },
    {
      name: 'Diminished sixth',
      short: 'd6',
      type: 'Diminished',
      group: 'diminished',
      order: 6
    }
  ],
  [
    {
      name: 'Minor sixth',
      short: 'm6',
      type: 'Minor',
      group: 'standard',
      order: 6
    },
    {
      name: 'Augmented fifth',
      short: 'A5',
      type: 'Augmented',
      group: 'augmented',
      order: 5
    }
  ],
  [
    {
      name: 'Major sixth',
      short: 'M6',
      type: 'Major',
      group: 'standard',
      order: 6
    },
    {
      name: 'Diminished seventh',
      short: 'd7',
      type: 'Diminished',
      group: 'diminished',
      order: 7
    }
  ],
  [
    {
      name: 'Minor seventh',
      short: 'm7',
      type: 'Minor',
      group: 'standard',
      order: 7
    },
    {
      name: 'Augmented sixth',
      short: 'A6',
      type: 'Augmented',
      group: 'augmented',
      order: 6
    }
  ],
  [
    {
      name: 'Major seventh',
      short: 'M7',
      type: 'Major',
      group: 'standard',
      order: 7
    },
    {
      name: 'Diminished octave',
      short: 'd8',
      type: 'Diminished',
      group: 'diminished',
      order: 8
    }
  ],
  [
    {
      name: 'Perfect octave',
      short: 'P8',
      type: 'Perfect',
      group: 'standard',
      order: 8
    },
    {
      name: 'Augmented seventh',
      short: 'A7',
      type: 'Augmented',
      group: 'augmented',
      order: 7
    },
    {
      name: 'Diminished ninth',
      short: 'd9',
      type: 'Diminished',
      group: 'diminished',
      order: 9
    }
  ],
  [
    {
      name: 'Minor ninth',
      short: 'm9',
      type: 'Minor',
      group: 'standard',
      order: 9
    },
    {
      name: 'Augmented octave',
      short: 'A8',
      type: 'Augmented',
      group: 'augmented',
      order: 8
    }
  ],
  [
    {
      name: 'Major ninth',
      short: 'M9',
      type: 'Major',
      group: 'standard',
      order: 9
    },
    {
      name: 'Diminished tenth',
      short: 'd10',
      type: 'Diminished',
      group: 'diminished',
      order: 10
    }
  ],
  [
    {
      name: 'Minor tenth',
      short: 'm10',
      type: 'Minor',
      group: 'standard',
      order: 10
    },
    {
      name: 'Augmented ninth',
      short: 'A9',
      type: 'Augmented',
      group: 'augmented',
      order: 9
    }
  ],
  [
    {
      name: 'Major tenth',
      short: 'M10',
      type: 'Major',
      group: 'standard',
      order: 10
    },
    {
      name: 'Diminished eleventh',
      short: 'd11',
      type: 'Diminished',
      group: 'diminished',
      order: 11
    }
  ],
  [
    {
      name: 'Perfect eleventh',
      short: 'P11',
      type: 'Perfect',
      group: 'standard',
      order: 11
    },
    {
      name: 'Augmented eleventh',
      short: 'A11',
      type: 'Augmented',
      group: 'augmented',
      order: 11
    },
    {
      name: 'Diminished twelfth',
      short: 'd12',
      type: 'Diminished',
      group: 'diminished',
      order: 12
    }
  ],
  [
    {
      name: 'Perfect twelfth',
      short: 'P12',
      type: 'Perfect',
      group: 'standard',
      order: 12
    },
    {
      name: 'Diminished thirteenth',
      short: 'd13',
      type: 'Diminished',
      group: 'diminished',
      order: 13
    }
  ],
  [
    {
      name: 'Minor thirteenth',
      short: 'm13',
      type: 'Minor',
      group: 'standard',
      order: 13
    },
    {
      name: 'Augmented twelfth',
      short: 'A12',
      type: 'Augmented',
      group: 'augmented',
      order: 12
    }
  ],
  [
    {
      name: 'Major thirteenth',
      short: 'M13',
      type: 'Major',
      group: 'standard',
      order: 13
    },
    {
      name: 'Diminished fourteenth',
      short: 'd14',
      type: 'Diminished',
      group: 'diminished',
      order: 14
    }
  ],
  [
    {
      name: 'Minor fourteenth',
      short: 'm14',
      type: 'Minor',
      group: 'standard',
      order: 14
    },
    {
      name: 'Augmented thirteenth',
      short: 'A13',
      type: 'Augmented',
      group: 'augmented',
      order: 13
    }
  ],
  [
    {
      name: 'Major fourteenth',
      short: 'M14',
      type: 'Major',
      group: 'standard',
      order: 14
    },
    {
      name: 'Diminished fifteenth',
      short: 'd15',
      type: 'Diminished',
      group: 'diminished',
      order: 15
    }
  ],
  [
    {
      name: 'Perfect fifteenth',
      short: 'P15',
      type: 'Perfect',
      group: 'standard',
      order: 15
    },
    {
      name: 'Augmented fourteenth',
      short: 'd14',
      type: 'Augmented',
      group: 'augmented',
      order: 14
    }
  ]
]
