import { chords } from './_chords'
const arpeggiatorAchievementsLockAmount = 8
const getArpeggiatorUnlockAmount = (index) => Math.floor(Math.pow(index * arpeggiatorAchievementsLockAmount, 1.2))

export const constants = {
  fretsTotal: 13,
  arpeggiatorAchievementsLockAmount,
  getArpeggiatorUnlockAmount,
  arpeggiatorAchievementsUnlockTable: chords.map((el, index) => getArpeggiatorUnlockAmount(index)),
  getArpeggiatorLastUnlock (amount) {
    return Math.max(this.arpeggiatorAchievementsUnlockTable.findIndex(el => el >= amount) - 1, 0)
  }
}
