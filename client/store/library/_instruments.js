export const instruments = [
  {
    name: 'guitar',
    strings: [6]
  },
  {
    name: 'ukulele',
    strings: [4]
  },
  {
    name: 'bass',
    strings: [4]
  },
  {
    name: 'mandolin',
    strings: [4]
  },
  {
    name: 'violin',
    strings: [4]
  }
]