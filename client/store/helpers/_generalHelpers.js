export const deepCopy = (obj) => JSON.parse(JSON.stringify(obj))

export const toRoman = (number) => {
  switch (number) {
    case 1: {
      return 'I'
    }
    case 2: {
      return 'II'
    }
    case 3: {
      return 'III'
    }
    case 4: {
      return 'IV'
    }
    case 5: {
      return 'V'
    }
    case 6: {
      return 'VI'
    }
    case 7: {
      return 'VII'
    }
    default: {
      return false
    }
  }
}
