import { octave } from '../library/_tones'

export const getChordNote = (chordRoot, rootIndex) => {
  return octave[(chordRoot + rootIndex) % 12]
}
// added rootIndex argument

export const getChordAll = (chordRoot, chordNotes, rootIndex) => {
  return chordNotes.map(thisNote => getChordNote(chordRoot + thisNote, rootIndex))
}

export const getScaleRoots = (chordType, currentChords) => {
  const tempChord = currentChords.find((el) => el.name === chordType.name)
  return tempChord ? tempChord.scaleRoots : []
}
// added currentChords argument

export const getChordObject = (chordRoot, chordType, rootIndex) => {
  return Object.assign({ root: getChordNote(chordRoot, rootIndex), allNotes: getChordAll(chordRoot, chordType.notes, rootIndex) }, chordType)
}

export const isSelectedChord = (chordRoot, chordType, selectedChord) => {
  const chordObject = getChordObject(chordRoot, chordType).chord
  return (chordObject.root === selectedChord.root && chordObject.name === selectedChord.name)
}
// added selectedChord argument


// flow getChordObject -> getChordAll -> getChordNote -> octave (need rootIndex here) 