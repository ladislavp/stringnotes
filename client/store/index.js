import Vue from 'vue'
import Vuex from 'vuex'
import { gameObjects, getScaleNotes, updateStatArchive } from './library/games'
import { library } from './library/_library'
const LZString = require('lz-string')
//localStorage.clear()

const storageGet = (key) => {
  const value = localStorage.getItem(key)
  return value && JSON.parse(LZString.decompress(value))
}
const storageSet = (key, item) => {
  if (key === 'gameObjects') {
    localStorage.setItem(key, LZString.compress(JSON.stringify([
      {
        id: 'arpeggiator',
        archive: item.archive,
        statsArchive: item.statsArchive,
        currentSetup: item.currentSetup
      },
      {
        id: 'fretboard',
        archive: item.archive,
        statsArchive: item.statsArchive,
        currentSetup: item.currentSetup
      }
    ])))
  }
  localStorage.setItem(key, LZString.compress(JSON.stringify(item)))
  return 0
}

Vue.use(Vuex)

const makeState = () => {
  const storageGameObjects = storageGet('gameObjects') || storageSet('gameObjects', gameObjects)

  return {
    count: 0,
    settings: {},
    guitarSound: {},
    currentGame: 'undefined',
    gameObjects: storageGameObjects ? gameObjects.map((el, index) => Object.assign({}, el, { archive: storageGameObjects[index].archive, statsArchive: storageGameObjects[index].statsArchive })) : gameObjects,
    library
  }
}

const state = makeState()

const mutations = {

  INCREMENT (state) {
    state.count++
  },
  DECREMENT (state) {
    state.count--
  },
  UPDATE_CURRENT_SETUP (state, payload) {

    switch (state.route.params.game) {
      case 'arpeggiator': {
        state.gameObjects.find(el => el.id === state.route.params.game).currentSetup = Object.assign({}, state.gameObjects.find(el => el.id === state.route.params.game).currentSetup, payload)
        break
      }
      case 'fretboard': {
        const currentSetup = state.gameObjects.find(el => el.id === state.route.params.game).currentSetup
        if (payload.scale) {
          const newNotes = getScaleNotes(payload.scale)
          // we will have old scale and either add or remove notes
          // first remove the ones that are not in old notes
          const newSelected = currentSetup.selected.filter(el => newNotes.findIndex(ell => el === ell) >= 0)
          const newSelectable = newNotes.filter(el => newSelected.findIndex(ell => el === ell) < 0)

          state.gameObjects.find(el => el.id === state.route.params.game).currentSetup = Object.assign({}, currentSetup, { scale: payload.scale, selected: newSelected, selectable: newSelectable })
        } else {
          state.gameObjects.find(el => el.id === state.route.params.game).currentSetup = Object.assign({}, currentSetup, payload)
        }
        break
      }
      default:
    }

    storageSet('gameObjects', state.gameObjects)
  },
  INIT_CURRENT_SETUP (state) {
    const tempObjects = storageGet('gameObjects')
    state.gameObjects.find(el => el.id === state.route.params.game).currentSetup = Object.assign({}, state.gameObjects.find(el => el.id === state.route.params.game).defaultSetup, tempObjects.find(el => el.id === state.route.params.game).currentSetup)
    
    storageSet('gameObjects', state.gameObjects)
  },
  UPDATE_APP_SETUP (state, payload) {
    state.settings = Object.assign({}, state.settings, payload)
    if (payload.tuning) {
      const tuningRoot = payload.tuning.root || 'E3'
      state.settings = Object.assign({}, state.settings,
        { tuning: Object.assign({}, payload.tuning, { root: library.tones.find(el => el.value === tuningRoot) }) })
    }
    storageSet('gameSettings', state.settings)
  },
  INIT_APP_SETUP (state) {
    // for now we ignore Banjo with weird string
    const tempSettings = storageGet('gameSettings')

    if (tempSettings) {
      state.settings = tempSettings
    } else {
      state.settings = {
        instrument: library.instruments[0],
        tuning: Object.assign({}, library.tunings[library.instruments[0].name][0], { root: library.tones.find(el => el.value === 'E3') }),
        volume: 40
      }
      storageSet('gameSettings', state.settings)
    }

  },
  GAME_USER_INPUT (state, input) {
    const gameObject = state.gameObjects.find(el => el.id === state.route.params.game)
    if(state.currentGame.status === 'loss') return 0
    state.currentGame = gameObject.turn(state.settings, gameObject.currentSetup, state.currentGame, input)
  },
  GAME_TICK (state) {
    switch (state.route.params.game) {
      case 'arpeggiator': {
        const gameObject = state.gameObjects.find(el => el.id === 'arpeggiator')
        state.currentGame = gameObject.turn(state.settings, gameObject.currentSetup, state.currentGame)
        if (state.currentGame.status === 'loss') {
          // state.gameObjects.find(el => el.id === 'arpeggiator').archive.find(el => ) state.currentGame
          // add totalcorrect, totalattempts, chordLength, accuracy to the state.currentGame
          const allTurns = state.currentGame.allTurns.concat(state.currentGame.turns)
          const currentChord = gameObject.currentSetup.chord
          state.currentGame.totalComplete = allTurns.length
          state.currentGame.totalAttempts = allTurns.reduce((acc, el) => acc + el.length, 0)
          state.currentGame.totalCorrect = allTurns.reduce((acc, el) => acc + el.filter(ell => ell.noteIndex >= 0).length, 0)
          state.currentGame.root = currentChord.root
          gameObject
            .archive[currentChord.name][currentChord.root]
            .push({
              root: state.currentGame.root,
              totalCorrect: state.currentGame.totalCorrect,
              totalAttempts: state.currentGame.totalAttempts,
              totalComplete: state.currentGame.totalComplete,
              gameLevel: state.currentGame.gameLevel,
              score: state.currentGame.score
            })
            //.push(state.currentGame)

          gameObject.statsArchive = updateStatArchive(gameObject.statsArchive, currentChord, state.currentGame, gameObject.archive)

          storageSet('gameObjects', state.gameObjects)
        }
        break
      }
      case 'fretboard': {
        const gameObject = state.gameObjects.find(el => el.id === 'fretboard')
        state.currentGame = gameObject.turn(state.settings, gameObject.currentSetup, state.currentGame)
        if (state.currentGame.status === 'loss') {
          // state.gameObjects.find(el => el.id === 'arpeggiator').archive.find(el => ) state.currentGame
          
          const allTurns = state.currentGame.allTurns.concat(state.currentGame.turns)
          state.currentGame.totalComplete = allTurns.length
          state.currentGame.totalAttempts = allTurns.reduce((acc, el) => acc + el.tries.length, 0)
          state.currentGame.totalCorrect = allTurns.filter(el => el.status === 'win').length

          gameObject
            .archive
            .push(state.currentGame)
          gameObject.statsArchive.totalScore += state.currentGame.score

          storageSet('gameObjects', state.gameObjects)
        }
        break
      }
      default:
    }
  },
  GAME_END (state) {
    switch (state.route.params.game) {
      case 'arpeggiator': {
        const gameObject = state.gameObjects.find(el => el.id === 'arpeggiator')
        const allTurns = state.currentGame.allTurns.concat(state.currentGame.turns)
        const currentChord = gameObject.currentSetup.chord
        state.currentGame.totalComplete = allTurns.length
        state.currentGame.totalAttempts = allTurns.reduce((acc, el) => acc + el.length, 0)
        state.currentGame.totalCorrect = allTurns.reduce((acc, el) => acc + el.filter(ell => ell.noteIndex >= 0).length, 0)
        state.currentGame.root = currentChord.root
        gameObject
          .archive[currentChord.name][currentChord.root]
          .push({
            root: state.currentGame.root,
            totalCorrect: state.currentGame.totalCorrect,
            totalAttempts: state.currentGame.totalAttempts,
            totalComplete: state.currentGame.totalComplete,
            gameLevel: state.currentGame.gameLevel,
            score: state.currentGame.score
          })
          //.push(state.currentGame)

        gameObject.statsArchive = updateStatArchive(gameObject.statsArchive, currentChord, state.currentGame, gameObject.archive)

        storageSet('gameObjects', state.gameObjects)
        break
      }
      case 'fretboard': {
        const gameObject = state.gameObjects.find(el => el.id === 'fretboard')
        const allTurns = state.currentGame.allTurns.concat(state.currentGame.turns)
        state.currentGame.totalComplete = allTurns.length
        state.currentGame.totalAttempts = allTurns.reduce((acc, el) => acc + el.tries.length, 0)
        state.currentGame.totalCorrect = allTurns.filter(el => el.status === 'win').length

        gameObject
          .archive
          .push(state.currentGame)
        gameObject.statsArchive.totalScore += state.currentGame.score

        storageSet('gameObjects', state.gameObjects)
        break
      }
      default:
    }

    state.currentGame = 'undefined'
  },
  GAME_RESET () {

    switch (state.route.params.game) {
      case 'arpeggiator': {
        const gameObject = state.gameObjects.find(el => el.id === 'arpeggiator')
        state.currentGame = gameObject.turn(state.settings, gameObject.currentSetup, state.currentGame, 'reset')
        break
      }
      case 'fretboard': {
        const gameObject = state.gameObjects.find(el => el.id === 'fretboard')
        state.currentGame = gameObject.turn(state.settings, gameObject.currentSetup, state.currentGame, 'reset')
        break
      }
      default:
    }
  },
  SET_GUITARSOUND (state, payload) {
    state.guitarSound = payload
  }
}

const Soundfont = require('soundfont-player')
const actions = {
  incrementAsync ({ commit }) {
    setTimeout(() => {
      commit('INCREMENT')
    }, 200)
  },
  makeSoundfont ({ commit }) {
    Soundfont.instrument(
      state.library.ac,
      'soundfonts/acoustic_guitar_steel-mp3.js'
    )
    .then((guitar) => {
      commit('SET_GUITARSOUND', guitar)
    })
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
