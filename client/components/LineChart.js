import { Line } from 'vue-chartjs'

export default Line.extend({
  name: 'LineChart',
  props: ['data', 'options'],
  mounted () {
    this.renderChart(this.data, this.options)
  }
})