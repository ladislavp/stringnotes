import { Bar} from 'vue-chartjs'

export default Bar.extend({
  name: 'BarChart',
  props: ['data', 'options'],
  mounted () {
    this.renderChart(this.data, this.options)
  }
})